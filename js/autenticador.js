const btnLimpiar = document.getElementById("btnLimpiar");
const email = document.getElementById("correo");
const pass = document.getElementById("pass");

const limpiar = () => {
  pass.value = "";
  correo.value = "";
};

btnLimpiar.addEventListener("click", limpiar);

// Import the functions you need from the SDKs you need
import { initializeApp } 
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
import {signInWithEmailAndPassword,getAuth,}
from "https://www.gstatic.com/firebasejs/9.13.0/firebase-auth.js";

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: "AIzaSyCqgdWAdt4LiyAMzEKZi0m4oFWFTpXuAZI",
    authDomain: "webfinal-f64c8.firebaseapp.com",
    databaseURL: "https://webfinal-f64c8-default-rtdb.firebaseio.com",
    projectId: "webfinal-f64c8",
    storageBucket: "webfinal-f64c8.appspot.com",
    messagingSenderId: "51519757531",
    appId: "1:51519757531:web:f2d6ec907d680d2eb840fe"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

window.onload = inicializar;
var formAutentificacion;

function inicializar() {
  formAutentificacion = document.getElementById("autentificacionf");
  formAutentificacion.addEventListener("submit", autentificar);
}

async function autentificar(event) {
  event.preventDefault();

  const email = event.target.correo.value;
  const password = event.target.pass.value;

  const auth = await getAuth();
  signInWithEmailAndPassword(auth, email, password)
    .then((userCredential) => {
      const user = userCredential.user;
      alert("Bienvenido al administrador");
      window.location.href = "/html/administrador.html";
    })
    .catch((error) => {
      const errorCode = error.code;
      const errorMessage = error.message;
      alert("Error Email o Password Incorrectos");
    });
}
