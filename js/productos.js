// Import the functions you need from the SDKs you need
import {
  initializeApp
} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";

import {
  getDatabase,
  onValue,
  ref,
} from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyCqgdWAdt4LiyAMzEKZi0m4oFWFTpXuAZI",
  authDomain: "webfinal-f64c8.firebaseapp.com",
  databaseURL: "https://webfinal-f64c8-default-rtdb.firebaseio.com",
  projectId: "webfinal-f64c8",
  storageBucket: "webfinal-f64c8.appspot.com",
  messagingSenderId: "51519757531",
  appId: "1:51519757531:web:f2d6ec907d680d2eb840fe"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();
const contenedorProductos = document.getElementById("contenedor--productos");
const templateProduct = document.getElementById("templateProduct");

function cardProducts(nombre, precio, editorial, fecha, url) {
  let template = templateProduct.content.cloneNode(true);
  let imagenProducto = template.querySelector("img");
  let nombreProducto = template.querySelector("h3");
  let precioProducto = template.querySelector("span.producto--precio");
  let descripcion = template.querySelector("span.producto--descripcion");

  imagenProducto.src = url;
  nombreProducto.innerHTML = nombre + "<hr/>";
  precioProducto.innerHTML = "$" + parseFloat(precio).toFixed(2) + " MXN";
  descripcion.innerHTML =
    "<br/>" + "Año de Lanzamiento: " + fecha;

  contenedorProductos.appendChild(template);
}

async function mostrarProductos() {
  const dbRef = ref(db, "productos");

  await onValue(
    dbRef,
    (snapshot) => {
      contenedorProductos.innerHTML = "";
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val();

        if (childData.status === 0) {
          cardProducts(
            childData.nombre,
            childData.precio,
            childData.editorial,
            childData.fecha,
            childData.imagen
          );
        }

        console.log(childKey + ":");
        console.log(childData.nombre);
      });
    }, {
      onlyOnce: true,
    }
  );
}

mostrarProductos();